# V39

## V39.20 (9c6233d6)
- enable GPIO control using PC13 on M0129 (aux power on-off)
- play init tone (from esc params) when received tone command with all zero values (0 power, duration and frequency)

## V39.19 (a62e20ac)
- test firmware, not released

## V39.18 (aec99de3)
- enable PWM outputs on all ESCs

## V39.17 (b1f28431)
- test firmware, not released

## V39.16 (d7682849)
- test version of PWM output feature
- fix frequency and duration mapping for the tone command so that it is consistent with the init tone params

## V39.15 (7b451410)
- send back the firmware minor version in the extended version packet

## V39 RC14 (8bdddd7a)
- add vtx gpio control to m0138 for testing

## V39 RC13 (eef79926)
- enable pwm input (mapped to % power) on all ESCs that support it (m0049, m0117, m0129, m0134)

## V39 RC12

## V39 RC11 (17d64675)
- experimental: improvements for low-kv motors. firmware posted for M0134-6 and M0138 only

## V39 RC10 (eb6fb500)
- update the current sense scale for M0117 and M0134 to more accurate value
- disable rgb led control via gpio since we don't have leds hooked up to any escs
- apply current limiting to power command as well as rpm command
- add trip current implementation using param `protection_trip_current_a`
- add support for M0138-1
- add over temp start-up limit and set it to 100C. Motor will not start spinning if temp > 100C.
- add inter-char timeout and set it to 20ms to reset the parser after bogus data may come in during firmware updates, etc
- update reported pwm value in feedback message during sinusoidal spin-up

## V39 RC9 (d4fe8e3d)
- [IMPORTANT] fix very rare de-sync bug due to timer overflow

## V39 RC8
- N/A

## V39 RC7 (25f8d49d)
- enable optional current limiting using param `protection_current_soft_limit_a`
- disable current limiting for M0129 mini ESC since there is no individual current sensing
- increase current sensing sampling frequency

## V39 RC6 (3d7ee4eb)
- add support for rpm div 2 comand to extend the rpm range from +/- 32.6K to +/- 65.4K

## V39 RC5 (fb6fd95c)
- change the uart tx buffer size back to 256 because 128 was not enough to send back params

## V39 RC4 (c0bf9200)
- clean up neopixel config to standardize across the boards and handle M0129 shared neopixel pin - only allow id 0 to control it

## V39 RC3
- add id-based addressing for neopixel led commands

## V39 RC2
- re-enable neopixel support on M0117 boards
- increase uart rx buffers to 256, reduce tx buffer to 128

## V39 RC0/RC1
- update max neopixel count to 32 and use dynamic packet size
- updates to neopixel rgb / rgbw support
- add new neopixel led interface


# V38
## V38 RC1 / FINAL (83faccfa), (07021c55 for M0134-6)
- add support for M0134-6

## V38 RC0 (7da4bb71)
- update the tune param structure to include spin-up params and others
- fix mosfet driver glitch during sinusoidal spinup
- add sanity checks on sinusoidal spinup params
- implement functionality for the stall check param `protection_stall_check_rpm`


# V37
## V37 RC2
- switch to using hardcoded board params for voltage and current scaling and offsets

## V37 RC1
- integrate M0129 battery status message

## V37 RC0
- improve sinusoidal spinup 
- make the sinusoidal spin-up default (currently no option to select old spin-up routine)
- add support for M0129-3
- add support for M0134-1 and M0134-3
- shorten the beeps and led blinks if there are bad params

# V36
- add optional sinusoidal spinup
- improve filtering for rpm calculation
- add power ramp limit in RPM / power mode in order to prevent super aggressive power transitions very quickly

# V35
- switch pwm input to map to RPM instead of PWM power.

# V34
- initial release
