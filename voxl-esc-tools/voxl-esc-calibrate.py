#!/usr/bin/python3

# Copyright (c) 2020 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# For a license to use on non-ModalAI hardware, please contact license@modalai.com

from voxl_esc_setup_paths import *
voxl_esc_setup_paths()

from libesc import *
from esc_scanner import EscScanner
import time
import numpy as np
import argparse

parser = argparse.ArgumentParser(description='ESC Calibration Script')
parser.add_argument('--device',              required=False, default=None)
parser.add_argument('--baud_rate',           required=False, default=None)
parser.add_argument('--id',        type=int, required=False, default=0)
parser.add_argument('--pwm-min',   type=int, required=False, default=10)
parser.add_argument('--pwm-max',   type=int, required=False, default=90)
parser.add_argument('--pwm-step',  type=int, required=False, default=3)
parser.add_argument('--pwm-step-duration',  type=float, required=False, default=0.5) #seconds
parser.add_argument('--pwm-step-transition',  type=float, required=False, default=0.4) #seconds
parser.add_argument('--temperature-limit', type=float, required=False, default=100.0)  #degrees C
parser.add_argument('--cmd-rate',    type=int,  required=False, default=250)
args = parser.parse_args()

devpath  = args.device
baudrate = args.baud_rate
esc_id   = args.id
PWM_MIN  = args.pwm_min
PWM_MAX  = args.pwm_max
PWM_STEP = args.pwm_step
STEPDURATION = args.pwm_step_duration
TRANSITIONTIME = args.pwm_step_transition
temperature_limit = args.temperature_limit
cmd_rate = args.cmd_rate
max_init_time = 3.0

# quick scan for ESCs to detect the port
scanner = EscScanner()
(devpath, baudrate) = scanner.scan(devpath, baudrate)

if devpath is not None and baudrate is not None:
    print('INFO: ESC(s) detected on port: ' + devpath + ', baud rate: ' + str(baudrate))
else:
    print('ERROR: No ESC(s) detected, exiting.')
    sys.exit(1)

#check input arguments
if PWM_MIN < 10 or PWM_MIN > 50:
    print('ERROR: Minimum power must be between 10 and 50')
    sys.exit(1)

if PWM_MAX < 10 or PWM_MAX > 100:
    print('ERROR: Maximum power must be between 10 and 100')
    sys.exit(1)

if PWM_MAX < PWM_MIN:
    print('ERROR: Maximum power must be greater than minimum power')
    sys.exit(1)

# PWM goal


# create ESC manager and search for ESCs
try:
    esc_manager = EscManager()
    esc_manager.open(devpath, baudrate)
except Exception as e:
    print('ERROR: Unable to connect to ESCs :')
    print(e)
    sys.exit(1)

# wait a little to let manager find all ESCs
time.sleep(0.25)
num_escs = len(esc_manager.get_escs())
if num_escs < 1:
    print('ERROR: No ESCs detected--exiting.')
    sys.exit(1)

esc = esc_manager.get_esc_by_id(esc_id)
if esc is None:
    print('ERROR: Specified ESC ID not found--exiting.')
    sys.exit(1)

# warn user
print('WARNING: ')
print('This test requires motors to spin at high speeds with')
print('propellers attached. Please ensure that appropriate')
print('protective equipment is being worn at all times and')
print('that the motor and propeller are adequately isolated')
print('from all persons.')
print('')
print('For best results, please perform this test at the')
print('nominal voltage for the battery used.')
print('')
response = input('Type "Yes" to continue: ')
if response not in ['yes', 'Yes', 'YES']:
    print('Test canceled by user')
    sys.exit(1)

# get ESC software version. sw_version < 20 means gen1 ESC, otherwise gen2
sw_version = esc.get_versions()[0]

esc_manager.enable_protocol_logging()
esc_manager.set_rx_sleep_time(1.0/cmd_rate)

# spin up
esc_manager.set_highspeed_fb(esc_id)  #tell ESC manager to only request feedback from this ID (so we get feedback 4x more often)
esc.set_target_power(10)
t_start = time.time()
print("Waiting for motor to spin up..")
while (time.time() - t_start) < max_init_time:
    time.sleep(0.01)
    esc_manager.send_pwm_targets()
    if (esc.get_status() == 5):  #status = 5 means that spin-up is finished
        break;

#wait a bit for the start-up transients to settle (voltage and current)
time.sleep(0.025)
esc_manager.send_pwm_targets()
time.sleep(0.025)

measurements     = []
all_measurements = []
t_test_start= time.time()

dt_desired  = 1.0/cmd_rate
t_next      = t_test_start + dt_desired
update_cntr = 0

# ramp up from min to max
pwm_now = PWM_MIN #10
while pwm_now <= PWM_MAX:
    esc.set_target_power(pwm_now)
    t_start = time.time()
    print('')
    while time.time() - t_start < STEPDURATION:
        #time.sleep(0.01)
        update_cntr   += 1
        t_now          = time.time()
        t_sleep        = (t_next - t_now)
        t_next        += dt_desired

        if (t_sleep > 0):
            time.sleep(t_sleep)
        t_after_sleep  = time.time()
        dt_after_sleep = t_after_sleep - t_now

        esc_manager.send_pwm_targets()


        board_voltage = esc_manager.get_board_voltage()
        board_current = esc_manager.get_board_current()

        esc_current   = esc.get_current()
        
        #ESCs that measure total board current do not have individual current sensing
        if board_current != None:
            esc_current = board_current

        if pwm_now >= PWM_MIN and time.time() - t_start >= TRANSITIONTIME:
            if 1: #update_cntr % (np.floor(cmd_rate/100)) == 0:  #save calibration data at 100hz
                measurements.append(
                    [(time.time()-t_test_start),esc.get_power(), esc.get_rpm(), esc.get_voltage(), esc_current])
                print('POW: %d, RPM: %.2f, Voltage: %.2fV, TEMPERATURE: %.2fC, Current: %.2fA' % (esc.get_power(), esc.get_rpm(), esc.get_voltage(), esc.get_temperature(), esc_current))

        if esc.get_temperature() > temperature_limit:
            print('ERROR: ESC %d overheated (%.2fC)!! Aborting.' % (esc.get_id(),esc.get_temperature()) )
            sys.exit(1)

        all_measurements.append(
                [(time.time()-t_test_start),esc.get_power(), esc.get_rpm(), esc.get_voltage(), esc_current])

    pwm_now += PWM_STEP

# close the manager and UART thread
esc_manager.close()
t_test_stop= time.time()
print('INFO: Test took %.2f seconds' % (t_test_stop-t_test_start))

# parse measurements
ts       = np.array([data[0] for data in measurements])
pwms     = np.array([data[1] for data in measurements])
rpms     = np.array([data[2] for data in measurements])
voltages = np.array([data[3] for data in measurements])
currents = np.array([data[4] for data in measurements])

all_ts       = np.array([data[0] for data in all_measurements])
all_rpms     = np.array([data[2] for data in all_measurements])
all_voltages = np.array([data[3] for data in all_measurements])
all_currents = np.array([data[4] for data in all_measurements])

#reported power is 0-100, but in ESC firmware it is 0-1000
motor_voltages = np.array([pwms[i]*10.0/999.0*(voltages[i]*1000) for i in range(len(pwms))])
#print motor_voltages

# linear fit or quadratic fit
ply = np.polyfit(rpms, motor_voltages, 2)
rpms_eval = np.array([x for x in range(np.max(rpms))])
motor_voltages_fit = np.polyval(ply, rpms_eval)
#motor_voltages_fit = np.polyval(ply, range(np.max(rpms)))

# print corresponding params
print('')
print('Quadratic fit: motor_voltage = a2*rpm_desired^2 + a1*rpm_desired + a0')
print('    pwm_vs_rpm_curve_a0 = ' + str(ply[2]))
print('    pwm_vs_rpm_curve_a1 = ' + str(ply[1]))
print('    pwm_vs_rpm_curve_a2 = ' + str(ply[0]))

try:
    import plotly.graph_objects as go
    from plotly.subplots import make_subplots
except:
    print('WARNING: In order to plot the results, install the Python "plotly" module: pip3 install plotly --upgrade')
    sys.exit(0)

#xplot = np.arange(len(rpm_log[0]))
subplot_specs =[[{}, {}],
                [{}, {"rowspan": 2}],
                [{}, None]]

fig = make_subplots(rows=3, cols=2, specs=subplot_specs, start_cell="top-left") #shared_xaxes=True
fig.add_trace(go.Scatter(x=rpms, y=motor_voltages/1000.0, name='Motor Voltage vs RPM', mode='markers'), row=1, col=1)
fig.add_trace(go.Scatter(x=rpms_eval, y=motor_voltages_fit/1000.0, name='Motor Voltage vs RPM (fit)'), row=1, col=1)
fig.add_trace(go.Scatter(x=pwms, y=rpms, name='RPM vs. PWM'), row=2, col=1)

fig.add_trace(go.Scatter(x=all_ts, y=all_voltages, name='Voltage vs. Time'), row=3, col=1)
fig.add_trace(go.Scatter(x=all_ts, y=all_currents, name='Current vs. Time'), row=1, col=2)
fig.add_trace(go.Scatter(x=ts, y=currents, name='Current vs. Time', mode='markers'), row=1, col=2)

fig.add_trace(go.Scatter(x=all_ts, y=all_rpms, name='RPM vs. Time'), row=2, col=2)
fig.add_trace(go.Scatter(x=ts, y=rpms, name='RPM vs. Time (sampled)', mode='markers'), row=2, col=2)

fig.update_layout(title_text='Voxl ESC Calibration Results')
fig.update_xaxes(title_text="RPM",row=1, col=1)
fig.update_yaxes(title_text="Commanded Motor Voltage (V)",row=1, col=1)
fig.update_xaxes(title_text="Power (%)",row=2, col=1)
fig.update_yaxes(title_text="RPM",row=2, col=1)

fig.update_xaxes(title_text="Time (s)",row=3, col=1)
fig.update_yaxes(title_text="Supply Voltage (V)",row=3, col=1)
fig.update_xaxes(title_text="Time (s)",row=1, col=2)
fig.update_yaxes(title_text="Current (A)",row=1, col=2)

fig.update_xaxes(title_text="Time (s)",row=2, col=2)
fig.update_yaxes(title_text="RPM",row=2, col=2)

#fig.update_layout(hovermode='x')

fig.update_xaxes(row=3, col=1, matches='x5')
fig.update_xaxes(row=1, col=2, matches='x5')
fig.update_xaxes(row=2, col=2, matches='x5')

fig.write_html('calibration_results.html',include_plotlyjs='cdn')
fig.show()
