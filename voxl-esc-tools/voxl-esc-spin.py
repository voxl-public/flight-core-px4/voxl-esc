#!/usr/bin/python3

# Copyright (c) 2020 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# For a license to use on non-ModalAI hardware, please contact license@modalai.com


from voxl_esc_setup_paths import *
voxl_esc_setup_paths()

from libesc import *
from esc_scanner import EscScanner
from esc_boards import *
import time
import numpy as np
import argparse


parser = argparse.ArgumentParser(description='ESC Test Spin Script')
parser.add_argument('--device',                 required=False, default=None)
parser.add_argument('--baud-rate',              required=False, default=None)
parser.add_argument('--id',          type=int,  required=True,  default=0)
parser.add_argument('--power',       type=int,  required=False, default=10)
parser.add_argument('--rpm',         type=int,  required=False, default=None)
parser.add_argument('--timeout',     type=float,  required=False, default=0.0)
parser.add_argument('--skip-prompt', type=str,  required=False, default='False')
parser.add_argument('--led-red',     type=int,  required=False, default=0)
parser.add_argument('--led-green',   type=int,  required=False, default=0)
parser.add_argument('--led-blue',    type=int,  required=False, default=0)
parser.add_argument('--cmd-rate',    type=int,  required=False, default=100)
parser.add_argument('--ramp-time',   type=float,required=False, default=0.0)
parser.add_argument('--init-time',   type=float,required=False, default=0.0)
parser.add_argument('--init-cmd',    type=float,required=False, default=0.0)
parser.add_argument('--sequential-spinup', type=float,required=False, default=0.0)
parser.add_argument('--enable-plot', type=int,  required=False, default=0)
parser.add_argument('--compare-rpms', type=int,  required=False, default=0)
parser.add_argument('--enable-logging', type=int,  required=False, default=0)
parser.add_argument('--enable-high-speed-feedback', type=int,  required=False, default=1)
parser.add_argument('--enable-debug',           required=False, default=0)
parser.add_argument('--enable-extended-rpm-cmd', required=False, default=0)
parser.add_argument('--temperature-limit', type=float, required=False, default=100.0)  #degrees C

args = parser.parse_args()

devpath  = args.device
baudrate = args.baud_rate
esc_id   = args.id
spin_pwr = args.power #0-100
spin_rpm = args.rpm #0-30000 .. limited to 30K for safety
timeout  = args.timeout
led_red  = int(args.led_red > 0)
led_green= int(args.led_green > 0)
led_blue = int(args.led_blue > 0)
cmd_rate = args.cmd_rate
ramp_time= args.ramp_time
init_time= args.init_time
init_cmd = args.init_cmd
seq_spin_delay = args.sequential_spinup
enable_plot = bool(args.enable_plot)
compare_rpm = bool(args.compare_rpms)
enable_logging = bool(args.enable_logging)
enable_high_speed_feedback = bool(args.enable_high_speed_feedback)
enable_extended_rpm_cmd = bool(args.enable_extended_rpm_cmd)
temperature_limit = args.temperature_limit
debug    = False
try:
    debug=args.enable_debug == 1 or 'True' in args.enable_debug or 'true' in args.enable_debug or '1' in args.enable_debug
except Exception as e:
    pass

MAX_SAFE_RPM = 40000

#optionally skip the safety prompt that asks to enter "yes" before spinning
skip_prompt = 'True' in args.skip_prompt or 'true' in args.skip_prompt

if spin_pwr < -100 or spin_pwr > 100:
    print('ERROR: Spin power must be between -100 and 100')
    sys.exit(1)

if spin_rpm is not None and (spin_rpm < -MAX_SAFE_RPM or spin_rpm > MAX_SAFE_RPM):
    print('ERROR: Spin rpm must be between %d and %d' % (-MAX_SAFE_RPM,MAX_SAFE_RPM))
    sys.exit(1)

if timeout < 0:
    print('ERROR: Timeout should be non-negative value of seconds')
    sys.exit(1)

if cmd_rate < 10:
    print('ERROR: Command rate is too low, the ESC will timeout')
    sys.exit(1)

if ramp_time < 0:
    print('ERROR: Ramp time has to be non-negative number.. provided %f' % ramp_time)
    sys.exit(1)

# quick scan for ESCs to detect the port
scanner = EscScanner()
if debug:
    scanner.enable_debug()
(devpath, baudrate) = scanner.scan(devpath, baudrate)

if devpath is not None and baudrate is not None:
    print('INFO: ESC(s) detected on port: ' + devpath + ', baud rate: ' + str(baudrate))
else:
    print('ERROR: No ESC(s) detected, exiting.')
    sys.exit(1)


# create ESC manager and search for ESCs
try:
    esc_manager = EscManager()
    if debug:
        esc_manager.enable_debug()
    esc_manager.open(devpath, baudrate)
except Exception as e:
    print('ERROR: Unable to connect to ESCs :')
    print(e)
    sys.exit(1)

# wait a little to let manager find all ESCs
time.sleep(0.25)
num_escs = len(esc_manager.get_escs())
if num_escs < 1:
    print('ERROR: No ESCs detected--exiting.')
    sys.exit(1)


for e in esc_manager.get_escs():
    versions      = e.get_versions()
    uid           = e.get_uid()
    fw_git_hash   = e.get_sw_git_hash()
    boot_git_hash = e.get_boot_git_hash()
    boot_version  = e.get_boot_version()
    hardware_name = get_esc_board_description(versions[1])

    if type(fw_git_hash) != str:
        fw_git_hash = fw_git_hash.decode()

    if type(boot_git_hash) != str:
        boot_git_hash = boot_git_hash.decode()

    print('\tID         : %d' % (e.get_id()))
    print('\tBoard      : version %d: %s' % (versions[1],hardware_name))
    #print('\tUID        : ' + '0x{}'.format(''.join(hex(x).lstrip("0x") for x in uid[::-1])))
    print('\tUID        : ' + '0x{}'.format(''.join("{0:02X}".format(x) for x in uid[::-1])))
    print('\tFirmware   : version %4d, hash %s' % (versions[0],fw_git_hash))
    print('\tBootloader : version %4d, hash %s' % (boot_version, boot_git_hash))
    print('')
print('---------------------')
print('')

escs = []

if esc_id != 255:
    esc = esc_manager.get_esc_by_id(esc_id)
    if esc is None:
        print('ERROR: Specified ESC ID not found--exiting.')
        sys.exit(1)
    escs.append(esc)
else:
    escs = esc_manager.get_escs()

# warn user
if not skip_prompt:
    print('WARNING: ')
    print('This test requires motors to spin at high speeds with')
    print('propellers attached. Please ensure that appropriate')
    print('protective equipment is being worn at all times and')
    print('that the motor and propeller are adequately isolated')
    print('from all persons.')
    print('')
    print('For best results, please perform this test at the')
    print('nominal voltage for the battery used.')
    print('')
    response = input('Type "Yes" to continue: ')
    if response not in ['yes', 'Yes', 'YES']:
        print('Test canceled by user')
        sys.exit(1)
else:
    print('WARNING: skipping user prompt to start the test due to override')
    print('')


if esc_id != 255 and enable_high_speed_feedback:
    esc_manager.set_highspeed_fb(esc_id)  #tell ESC manager to only request feedback from this ID (so we get feedback 4x more often)

for esc in escs:
    esc.set_leds([led_red, led_green, led_blue])  #0 or 1 for R G and B values.. binary for now

esc_manager.enable_debug_msgs()

if enable_logging:
    esc_manager.enable_protocol_logging()

#new message that extends the RPM range from +/- 32.7K to 65.4K RPM (supported on firmware v39_RC6 or later)
#this is not needed for most applications
if enable_extended_rpm_cmd:
    #check esc firmware version
    for e in esc_manager.get_escs():
        if e.get_versions()[0] < 39:
            print('ERROR: extended RPM command is only supported on firmware v39_RC6 or later')
            sys.exit(1)

    print('\nWARNING: extended RPM command is only supported on firmware v39_RC6 or later\n')

    time.sleep(1.0)
    esc_manager.set_rpm_cmd_type_div2()

#esc_manager.set_rx_sleep_time(0.001)
esc_manager.set_rx_sleep_time(1.0/cmd_rate)

update_cntr = 0
t_start     = time.time()
dt_desired  = 1.0/cmd_rate
t_next      = t_start + dt_desired

mean_rpm    = [0.0, 0.0, 0.0, 0.0]
mean_rpm    = np.array(mean_rpm)

rpm_ts      = [[],[],[],[]];
rpm_log     = [[],[],[],[]];
cmd_log     = [[],[],[],[]];
volt_log    = [[],[],[],[]];
curr_log    = [[],[],[],[]];
temp_log    = [[],[],[],[]];
board_curr_log = []
board_curr_ts_log = []

while timeout == 0 or time.time() - t_start < timeout:
    update_cntr   += 1
    t_now          = time.time()
    t_sleep        = (t_next - t_now)
    t_next        += dt_desired

    if (t_sleep > 0):
        time.sleep(t_sleep)
    t_after_sleep  = time.time()
    dt_after_sleep = t_after_sleep - t_now

    #time.sleep(1.0/cmd_rate)
    #print('t_sleep = %f, dt after sleep = %f' % (t_sleep, dt_after_sleep))

    if (ramp_time == 0.0):
        ramp_percentage = 1.0
    else:
        ramp_percentage = (t_now-t_start-init_time)/ramp_time
        if ramp_percentage > 1.0:
            ramp_percentage = 1.0
        if ramp_percentage < 0.0:
            ramp_percentage = 0.0
        #ramp_percentage = 1.0


    if spin_rpm is not None:
        spin_rpm_ramp = spin_rpm * ramp_percentage;
        for esc in escs:
            if ((t_now - t_start) < esc.get_id() * seq_spin_delay):
                esc.set_target_rpm(0)
            else:
                esc.set_target_rpm(spin_rpm_ramp)
        esc_manager.send_rpm_targets()
    else:
        if init_time != 0:
            if t_now - t_start < init_time:
                spin_pwr_ramp = init_cmd
            else:
                spin_pwr_ramp = spin_pwr * ramp_percentage;
                if spin_pwr != 0 and abs(spin_pwr_ramp) < 5:
                    spin_pwr_ramp = np.sign(spin_pwr)*5
        else:
            spin_pwr_ramp = spin_pwr * ramp_percentage;
            if spin_pwr != 0 and abs(spin_pwr_ramp) < 5:
                spin_pwr_ramp = np.sign(spin_pwr)*5
        

        #spin_pwr_ramp = spin_pwr_out * ramp_percentage;
        #print(spin_pwr_ramp)
        for esc in escs:
            if ((t_now - t_start) < esc.get_id() * seq_spin_delay):
                esc.set_target_power(0)
            else:
                esc.set_target_power(spin_pwr_ramp)
        esc_manager.send_pwm_targets()

    board_voltage = esc_manager.get_board_voltage()
    board_current = esc_manager.get_board_current()

    for esc in escs:
        if esc.get_rpm() > 0:
            mean_rpm[esc.get_id()] = mean_rpm[esc.get_id()]*0.95 + esc.get_rpm()*0.05

        esc_id = esc.get_id()

        #if update_cntr > 100:  #skip a few initial iterations because the feedback does not come back right away and we end up saving zeros..
        if esc.get_last_time() != 0: #wait for feedback data to come in
            rpm_log[esc_id].append(esc.get_rpm())
            cmd_log[esc_id].append(esc.get_power())
            volt_log[esc_id].append(esc.get_voltage())
            curr_log[esc_id].append(esc.get_current())
            temp_log[esc_id].append(esc.get_temperature())
            rpm_ts[esc_id].append(t_now - t_start)

            if (esc.get_temperature() > temperature_limit):
                print('ERROR: ESC %d overheated (%.2fC)!! Aborting.' % (esc_id,esc.get_temperature()) )
                timeout = 1.0
                #sys.exit(1)

            if board_current != None:  #FIXME: should this be moved out of the esc loop?
                board_curr_log.append(board_current)
                board_curr_ts_log.append(t_now - t_start)

        if update_cntr % (np.floor(cmd_rate/50)) == 0:
            if board_voltage != None and board_current != None:
                print('[%.3f] (%d) RPM: %.0f, PWR: %.0f, VOLTAGE: %.3fV, TEMPERATURE: %.2fC, CURRENT: %.3fA, BOARD_VOLTAGE: %.3fV, BOARD_CURRENT: %.3fA' % 
                    (t_now - t_start,esc.get_id(), esc.get_rpm(), esc.get_power(), esc.get_voltage(), esc.get_temperature(), esc.get_current(), board_voltage, board_current))
            else:
                print('[%.3f] (%d) RPM: %.0f, PWR: %.0f, VOLTAGE: %.3fV, TEMPERATURE: %.2fC, CURRENT: %.3fA' % (t_now - t_start,esc.get_id(), esc.get_rpm(), esc.get_power(), esc.get_voltage(), esc.get_temperature(), esc.get_current()))


    #if update_cntr % 10 == 0:
    #    print('[%f] TX=%d, RX=%d packets, RX CRC ERRORS=%d' % (t_now - t_start, esc_manager.tx_packet_count, esc_manager.rx_packet_count, esc_manager.protocol.crc_error_count))


print('Finished!')
print('[%f] TX=%d, RX=%d packets, RX CRC ERRORS=%d' % (t_now - t_start, esc_manager.tx_packet_count, esc_manager.rx_packet_count, esc_manager.protocol.crc_error_count))
print('Average RPMs: %.2f %.2f %.2f %.2f' % (mean_rpm[0],mean_rpm[1],mean_rpm[2],mean_rpm[3]))
print('Average RPM deviation between ESCs : %.2f' %(mean_rpm.max()-mean_rpm.min()))

MAX_RPM_DIFF = 200
#print(compare_rpm)
if compare_rpm:
    if (mean_rpm.max()-mean_rpm.min()) < MAX_RPM_DIFF and (mean_rpm.min() > 0):
        print("RPM_COMPARISON TEST PASSED")
    else:
        print("RPM COMPARISON TEST FAILED")


# plot results if possible
if enable_plot:
    try:
      import plotly.graph_objects as go
      from plotly.subplots import make_subplots
    except:
      print('WARNING: In order to plot the results, install the Python "plotly" module: pip3 install plotly --upgrade')
      sys.exit(0)


    #xplot = np.arange(len(rpm_log[0]))
    #xplot = rpm_ts[idx]

    #fig = go.Figure()
    fig = make_subplots(rows=5, cols=1, start_cell="top-left") #shared_xaxes=True
    for idx in range(4):
        fig.add_trace(go.Scatter(x=rpm_ts[idx], y=np.array(rpm_log[idx]), name='RPM #%d'%idx), row=1, col=1)
        fig.add_trace(go.Scatter(x=rpm_ts[idx], y=np.array(cmd_log[idx]), name='Command #%d'%idx), row=2, col=1)
        fig.add_trace(go.Scatter(x=rpm_ts[idx], y=np.array(volt_log[idx]), name='Voltage #%d (V)' % idx),row=3, col=1)
        fig.add_trace(go.Scatter(x=rpm_ts[idx], y=np.array(curr_log[idx]), name='Current #%d (A)' % idx),row=4, col=1)
        fig.add_trace(go.Scatter(x=rpm_ts[idx], y=np.array(temp_log[idx]), name='Temperature #%d (deg C)' % idx),row=5, col=1)

    if board_curr_log:
        fig.add_trace(go.Scatter(x=board_curr_ts_log, y=np.array(board_curr_log), name='Total Current (A)'),row=4, col=1)
    

    fig.update_layout(title_text='Voxl ESC Spin Test')
    fig.update_xaxes(title_text="Time (s)",row=1, col=1)
    fig.update_yaxes(title_text="Reported RPM",row=1, col=1)
    fig.update_xaxes(title_text="Time (s)",row=2, col=1)
    fig.update_yaxes(title_text="ESC Command (%)",row=2, col=1)
    fig.update_xaxes(title_text="Time (s)",row=3, col=1)
    fig.update_yaxes(title_text="Voltage (V)",row=3, col=1)
    fig.update_xaxes(title_text="Time (s)",row=4, col=1)
    fig.update_yaxes(title_text="Current (A)",row=4, col=1)
    fig.update_xaxes(title_text="Time (s)",row=5, col=1)
    fig.update_yaxes(title_text="Temperature (deg C)",row=5, col=1)
    fig.update_xaxes(matches='x')

    fig.write_html('voxl_esc_spin_results.html',include_plotlyjs='cdn')
    fig.show()
