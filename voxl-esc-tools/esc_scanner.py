# Copyright (c) 2020 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# For a license to use on non-ModalAI hardware, please contact license@modalai.com

import os
import platform
import subprocess
from libesc import *
from libesc.escmanager import *
import json
import re
import sys

class EscScanner:
    
    def __init__(self):
        self.system = None
        self.system               = None
        self.port_candidates      = []
        self.firmware_baudrates   = [2000000, 250000, 921600, 230400, 57600, 115200] #6000000
        self.bootloader_baudrates = [230400]
        self.is_voxl              = False
        self.voxl_platform        = ''
        self.uart_port_desc       = ['uart','serial','vcp','ttl232','stlink']
        self.protocol_type        = None
        self.try_prev_connection  = False
        self.prev_connection_file_name = '.voxl_esc_cache'
        self.debug                = False

    def detect_voxl(self):
        try:
            ret = subprocess.check_output(['voxl-platform'])
            #print(ret.decode())

            self.is_voxl       = True
            self.voxl_platform = ret.decode()
            self.voxl_platform = re.sub(r"[\n\t\s]*", "", self.voxl_platform) #strip whitespaces
        except:
            pass

    def is_voxl_px4_running(self):
        try:
            ret = subprocess.check_output(['systemctl', 'is-active', 'voxl-px4'])
            #ret = subprocess.check_output(['systemctl', 'status', 'voxl-px4'])
            #print(ret)
            if 'active' in str(ret):
                #print('running!')
                return True
            else:
                #print('not running!')
                return False
        
        except:
            pass

        return False

        

    def enable_debug(self):
        self.debug = True

    def get_protocol(self):
        return self.protocol_type

    def scan(self, device=None, baudrate=None):

        if (device is not None and baudrate is not None):
            self.try_prev_connection = False

        if (baudrate is not None):
            self.firmware_baudrates = [baudrate]

        if device is None:
            self.system = platform.system()  # detect OS
            self.detect_voxl()               # detect VOXL

            if self.is_voxl:
                print("VOXL Platform: %s" % (self.voxl_platform))
                if 'M0052' in self.voxl_platform or 'M0054' in self.voxl_platform or 'M0104' in self.voxl_platform:
                    print('Detected RB5 Flight, VOXL2 M0054 or M0104!')
                    self.port_candidates = ['/dev/slpi-uart-2']

                    if self.is_voxl_px4_running():
                        print('ERROR: voxl-px4 is running, please stop it using comand:')
                        print('\tsystemctl stop voxl-px4')
                        sys.exit(1)
                else:
                    #voxl1 ports..
                    print('Detected unknown VOXL platform.. VOXL1?')
                    self.port_candidates = ['/dev/slpi-uart-7','/dev/slpi-uart-5','/dev/slpi-uart-12','/dev/slpi-uart-9']
            elif self.system == 'Windows':
                self.port_candidates = ['COM%s' % (i + 1) for i in range(100)]
            elif self.system == 'Linux' or self.system == 'Darwin':
                import serial.tools.list_ports as portlist
                self.ports = portlist.comports()
                print('INFO: All COM ports:')
                for pt in self.ports:
                    print("\t" + str(pt.device) + " : " + str(pt.description))

                    # see if any keywords in port description match
                    for desc in self.uart_port_desc:
                        if desc in pt.description.lower():
                            self.port_candidates.append(pt.device)
                            break

                print('INFO: UART Port Candidates:')
                for pt in self.port_candidates:
                    print("\t" + pt)
            else:
                raise Exception('Unsupported OS')

        else:
            self.port_candidates = [device]

        if self.try_prev_connection:
            #try to use previous connection information to speed up scanning
            prev_dev       = None
            prev_baud_rate = None
            try:
                with open(self.prev_connection_file_name, 'r') as openfile:
                    json_object = json.load(openfile)

                prev_dev = json_object['device'];
                prev_baud_rate = int(json_object['baud-rate'])
            except Exception as e:
                #print(e)
                pass


            if prev_dev is not None and prev_dev in self.port_candidates:
                try:
                    self.firmware_baudrates.remove(prev_baud_rate)
                    self.port_candidates.remove(prev_dev)
                except:
                    pass
                self.firmware_baudrates.insert(0,prev_baud_rate)
                self.port_candidates.insert(0,prev_dev)
                print('Found previous connection information in %s ..' % self.prev_connection_file_name)
                print('Prioritizing %s @ %d' % (prev_dev,prev_baud_rate))
                #print(self.firmware_baudrates)
                #print(self.port_candidates)

        # check for ESCs in firmware application
        for port in self.port_candidates:
            for baud in self.firmware_baudrates:
                print('INFO: Scanning for ESC firmware: ' + port + ', baud: ' + str(baud))

                try:                             # try to open serial port
                    esc_manager = EscManager()
                    if self.debug:
                        esc_manager.enable_debug()
                    esc_manager.open(port, baud)
                except Exception as e:
                    print(e)
                    continue

                # request firmware version and see if there is a response
                #time.sleep(0.025)
                escs = esc_manager.get_escs()

                if len(escs) > 0:
                    esc_manager.close()
                    self.protocol_type = 'firmware'

                    #save connection information to file for faster scan next time
                    connection_info = {
                        "device"    : port,
                        "baud-rate" : baud
                    }

                    json_object = json.dumps(connection_info, indent=4)

                    try:
                        with open(self.prev_connection_file_name, "w") as outfile:
                            outfile.write(json_object)
                    except Exception as e:
                        print(e)
                        #pass

                    return (port, baud)

                esc_manager.close()

        # check for ESCs in bootloader
        for port in self.port_candidates:
            for baud in self.bootloader_baudrates:
                print('INFO: Scanning for ESC bootloader: ' + port + ', baud: ' + str(baud))

                try:                              # try to open serial port
                    esc_manager = EscManager()
                    if self.debug:
                        esc_manager.enable_debug()
                    esc_manager.open(port, baud)
                    esc_manager.set_protocol(types.ESC_PROTOCOL_BOOTLOADER)
                    esc_manager.set_baudrate(baud)
                except Exception as e:
                    print(e)
                    continue

                # request bootloader version and see if there is a response
                time.sleep(0.025)
                if esc_manager.detect_bootloader():
                    esc_manager.close()
                    self.protocol_type = 'bootloader'
                    return (port, baud)

                esc_manager.close()

        return (None, None) #no ESCs detected


if __name__ == "__main__":
    scanner = EscScanner()
    scanner.scan()
