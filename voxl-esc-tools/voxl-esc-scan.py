#!/usr/bin/python3

# Copyright (c) 2020 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# For a license to use on non-ModalAI hardware, please contact license@modalai.com

from voxl_esc_setup_paths import *
voxl_esc_setup_paths()

from libesc import *
from esc_scanner import EscScanner
from esc_boards import *
import argparse
from itertools import repeat

parser = argparse.ArgumentParser(description='ESC Scan Script')
parser.add_argument('--device',                 required=False, default=None)
parser.add_argument('--baud-rate',              required=False, default=None)
parser.add_argument('--enable-debug',           required=False, default=0)
parser.add_argument('--force-scan-all-baud-rates', required=False, default=0)
parser.add_argument('--params-search-path', type=str, required=False, default='../voxl-esc-params')
parser.add_argument('--verify-params',           required=False, default=0)
args = parser.parse_args()

devpath  = args.device
baudrate = args.baud_rate
protocol = 'None'
debug    = False
force_scan_all_baud_rates = False
params_search_path = args.params_search_path
verify_params = int(args.verify_params)

try:
    debug=args.enable_debug == 1 or 'True' in args.enable_debug \
        or 'true' in args.enable_debug or '1' in args.enable_debug
except Exception as e:
    pass

try:
    force_scan_all_baud_rates = args.force_scan_all_baud_rates == 1 \
        or 'True' in args.force_scan_all_baud_rates \
        or 'true' in args.force_scan_all_baud_rates \
        or '1' in args.force_scan_all_baud_rates
except Exception as e:
    #print(e)
    pass


esc_ids_found = [0,0,0,0]
esc_firmware_hashes = ['','','','',]
esc_found_baud_rate_count = 0


if baudrate is None or force_scan_all_baud_rates:
    firmware_baudrates   = [2000000, 250000, 921600, 57600]
else:
    firmware_baudrates = [baudrate]

esc_info = []


# iterate over all baud rates (unless forced to a single baud rate)
# if iterating over baud rate list, the loop will exit when all ESCs are found
for b in firmware_baudrates:
    baudrate_test = b
    devpath_test  = args.device

    scanner = EscScanner()
    if debug:
        scanner.enable_debug()
    (devpath, baudrate) = scanner.scan(devpath_test, baudrate_test)

    if devpath is not None and baudrate is not None:
        protocol = scanner.get_protocol()
        print('INFO: ESC(s) detected on port: ' + devpath + ', baud rate: ' + str(baudrate) + ', protocol: ' + protocol)
    else:
        print('WARNING: No ESC(s) detected')
        #sys.exit(1)
        continue

    esc_found_baud_rate_count +=1

    try:
        esc_manager = EscManager()
        if debug:
            esc_manager.enable_debug()
        esc_manager.open(devpath, baudrate)
    except Exception as e:
        print('ERROR: Unable to connect to ESCs :')
        print(e)
        sys.exit(1)

    # wait a little to let manager find all ESCs
    #time.sleep(0.35)

    # verify params (if not disabled)
    params_status=['unknown']*4
    params_match_error_str = ['']*4
    num_invalid_params = 0
    num_exact_match    = 0

    if verify_params == 1:

        def wait_for_params(e, timeout, param_type):
            t_start = time.time()
            while (time.time()-t_start < timeout):
                if param_type == 'id' and e.params.is_valid_id():
                    return True
                if param_type == 'board' and e.params.is_valid_board():
                    return True
                if param_type == 'uart' and e.params.is_valid_uart():
                    return True
                if param_type == 'tune' and e.params.is_valid_tune():
                    return True
                    
                time.sleep(0.005)

            #print('param timeout (%s)' % param_type)
            #sys.exit(1)
            return False


        print('INFO: Verifying ESC Params..')
        for e in esc_manager.get_escs():
            param_timeout = 0.200
            esc_manager.request_config_id(e.get_id())
            wait_for_params(e,param_timeout,'id');
            esc_manager.request_config_board(e.get_id())
            wait_for_params(e,param_timeout,'board');
            esc_manager.request_config_uart(e.get_id())
            wait_for_params(e,param_timeout,'uart');
            esc_manager.request_config_tune(e.get_id())
            wait_for_params(e,param_timeout,'tune');

            # wait for the params responses to come back
            #time.sleep(0.050)

            if not e.params.is_valid():
                print('ERROR: Params for ID %d are invalid!' % (e.get_id()))
                params_status[e.get_id()] = 'invalid'
                num_invalid_params +=1
            else:
                params_status[e.get_id()] = 'valid'
                # get the params as XML string and save to a file
                params_str = e.params.get_xml_string()

                # try to find a match with existing xml file in params directory
                params_candidates = glob.glob(params_search_path + '/**/*.xml', recursive=True)

                for param_file in params_candidates:
                    try:
                        with open(param_file, 'r') as f:
                            temp_param_str = f.read()
                        try_params = params_from_xml(temp_param_str)
                        if try_params.get_param_bytes_all() == e.params.get_param_bytes_all():
                            #print('INFO: Params from ID %d match %s' % (esc_id,param_file))
                            params_status[e.get_id()] = param_file
                            num_exact_match += 1
                    except Exception as e:
                        print('ERROR: Unable to open param file %s' % (param_file))
                        print(e)

        #compare all params from all ESCs
        num_params_match = 0;
        esc_ids = [e.get_id() for e in esc_manager.get_escs()]
        for esc_id in esc_ids:
            if esc_manager.get_esc_by_id(esc_ids[0]).params.get_param_bytes_all() != \
                esc_manager.get_esc_by_id(esc_id).params.get_param_bytes_all():
                #print('ERROR: Params from ID %d and %d are not the same' % (esc_ids[0],esc_id))
                params_match_error_str[e.get_id()] = '(match FAILED)'
            else:
                num_params_match += 1

    print('\n')
    print('INFO: ESC Information:')
    print('INFO: ---------------------')

    for e in esc_manager.get_escs():
        esc_ids_found[e.get_id()] = 1
        esc_info.append([e.get_id(), devpath, baudrate])
        versions      = e.get_versions()
        uid           = e.get_uid()
        fw_git_hash   = e.get_sw_git_hash()
        boot_git_hash = e.get_boot_git_hash()
        boot_version  = e.get_boot_version()
        hardware_name = get_esc_board_description(versions[1])

        if type(fw_git_hash) != str:
            fw_git_hash = fw_git_hash.decode()

        if type(boot_git_hash) != str:
            boot_git_hash = boot_git_hash.decode()

        esc_firmware_hashes[e.get_id()] = fw_git_hash

        print('\tID         : %d' % (e.get_id()))
        print('\tBoard      : version %d: %s' % (versions[1],hardware_name))
        #print('\tUID        : ' + '0x{}'.format(''.join(hex(x).lstrip("0x") for x in uid[::-1])))
        print('\tUID        : ' + '0x{}'.format(''.join("{0:02X}".format(x) for x in uid[::-1])))
        print('\tFirmware   : version %4d, hash %s' % (versions[0],fw_git_hash))
        print('\tBootloader : version %4d, hash %s' % (boot_version, boot_git_hash))
        if verify_params == 1:
            print('\tParams     : %s %s' % (params_status[e.get_id()], params_match_error_str[e.get_id()]))
        print('')
    print('---------------------')

    if baudrate == 57600:
        print('\nWARNING: 57600 baud rate is the fallback baud rate if ESC params are not valid')
        print('WARNING: Please verify params using voxl-esc-verify-params.py')
        print('WARNING: Motors will not spin unless valid params are present')
        print('---------------------')

    if verify_params == 1 and num_params_match != 4:
        print('\nWARNING: not all params match!')
        print('---------------------')

    esc_manager.close()

    if all(esc_ids_found):
        break

if esc_found_baud_rate_count > 1: #force_scan_all_baud_rates:
    print('\nFound ESCs:')
    for e in esc_info:
        print('id: %d, port: %s, baud rate: %d' % (e[0], e[1], e[2]))

#check if all the firmware hashes are the same
if  esc_firmware_hashes != list(repeat(esc_firmware_hashes[0], len(esc_firmware_hashes))):
    print('\nERROR: not all ESC firmware hashes are the same')
    sys.exit(1)

if len(esc_info) != 4:
    print('\nERROR: not all ESCs found')
    for e in esc_info:
        print('id: %d, port: %s, baud rate: %d' % (e[0], e[1], e[2]))
    sys.exit(1)


