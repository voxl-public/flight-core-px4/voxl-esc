# Regenerative Braking

## Background
Regenerative braking is one of methods used by ESCs to reduce the speed of a spinning motor. This approach converts the mechanical
energy stored in the spinning motor (and propeller) into electrical energy and heat. For applications like UAVs, 
the amount of energy generated from braking is negligible, however this method has an advantage of quickly slowing down
the motor (as opposed to motor just coasting freely). In absence of regenerative braking, the motor and propeller would
only be slowed down by the friction in the bearings and air drag of motor and propeller. However, for UAV motor control,
fast response of the ESC + motor is desired, thefore it is an advantage to use regenrative braking to speed up motor deceleration.

Regenerative braking converts mechanical energy to electrical and that energy (current) has to flow somewhere. When the ESC is connected to a battery,
the battery is charged during regenerative braking. During this time, the current is flowing from the motor into the battery via the mosfets in the ESC.
This is possible because the battery can sink current as well as source current. However, if a power supply is used with regenerative braking,
issues can arise.

Most power supplies can only source current but not sink (due to their design). This means that during generative braking event, the
generated electrical energy (current) has nowhere to flow except for the bulk capacitors on the ESC and the capacitors inside the power supply. 
As the capacitors are accepting the generated energy, the voltage rises. The voltage rise can be very significant and can damage components 
on the ESC because all components have maximum voltage rating (typically 20-35V for 4S-rated ESC and 35-40V for 6S-rated ESC)

Modal AI ESCs (M0049, M0117, M0129, M0134) have a built in TVS diode (Transient Voltage Suppression diode), which is designed to suppress
voltage spikes of short duration. This diode can protect against spikes when testing with a power supply. The diode works by clamping the voltage
at certain level (lets say 20V for a 4S-rated ESC), so that the current flows through the diode - this prevents the voltage from rising further.
However, current flowing through the TVS diode will make the diode dissipate heat, so if the power spike has a substantial magnitude and duration,
too much heat in the TVS diode will cause it to be damaged (and potentially burn up).

## Test Results

In order to demonstrate the effect of regenerative braking, we ran some tests with a power supply (12V) and a 3S Lipo Battery.
The tests were executed by commanding ESC to alternate between two power set points (for example between 10 and 20 percent power).

The test platform details are:

- M0129-3 ESC
- 12V power supply and 3S Lipo Battery (around 12V)
- EMAX RS1306 3300kV motors (no propellers)
- M0129 ESC has a built-in TVS diode that clamps around 19-21V. The other ESC components are rated to at least 25V

The results can be summarised as follows:

- all tests with a power supply show a positive voltage spike and negative current spike during braking
- negative current means the current is flowing out of the ESC and charging the capacitor in the power supply (or battery)
- the larger the change in power / rpm is (amount of braking), the larger the regenerative voltage spike is
- in tests with a power supply, the TVS diode clamps the voltage spike around 20-21 V, as evident from the shape of the voltage spike
- the magnitude of voltage spike in tests with a power supply is almost 10V, whereas with a battery, the spike is only 0.2V.
- batteries have small internal resistance, so a small voltage increase during regenerative braking is normal (battery is charging)
- deceleration of the motor is a lot faster with battery (compared to power supply). This can be seen from the RPM plot and the reason is that the battery is providing a larger load to the ESC / motor,
so it is harder for motor to spin, causing it to decelerate faster

## Warnings
- TODO

## Conclusions
- Testing with a power supply can damage ESC components under certain conditions and should be done by advanced users only
- When testing with a power supply, always monitor the voltage spikes and experiment with less aggressive commands first to make sure the voltage spikes are not going to damage the ESC

Test Command:
```
./voxl-esc-spin-step.py --power 10 --step-amplitude 30 --cmd-rate 1000 --id 255 --timeout 2 --enable-plot 1
```

### (Power Supply) Spin Test Results (Step between 10% and 20% power)
![Step test 10-20% (power supply)](img/regen_test_m0129_10_20_power_supply.png)

### (Power Supply) Spin Test Results (Step between 10% and 30% power)
![Step test 10-30% (power supply)](img/regen_test_m0129_10_30_power_supply.png)

### (Power Supply) Spin Test Results (Step between 10% and 40% power)
![Step test 10-40% (power supply)](img/regen_test_m0129_10_40_power_supply.png)

### (Battery) Spin Test Results (Step between 10% and 40% power)
![Step test 10-40% (power supply)](img/regen_test_m0129_10_40_battery.png)
