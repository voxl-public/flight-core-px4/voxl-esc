#!/bin/bash


#DEVICE="/dev/ttyUSB0"
#BAUD="2000000"
POWER="10"
TIMEOUT="5"


echo "--- voxl esc spin helper ---"
echo "WARNING: All motors will SPIN." 
echo "Please remove all propellers, then press ENTER key to proceed . . . "

#wait here
read

#use Python 3.6+
for mot in {0..3}
do
#   python3 voxl-esc-spin.py --device $DEVICE --baud-rate $BAUD --id $mot --power $POWER --timeout $TIMEOUT --skip-prompt True
    python3 voxl-esc-spin.py --id $mot --power $POWER --timeout $TIMEOUT --skip-prompt True
done
