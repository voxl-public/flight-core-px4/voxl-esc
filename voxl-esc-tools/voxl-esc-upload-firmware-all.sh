#!/bin/bash

#set -e
#for ESC_ID in {0..3}
#do
#   ./voxl-esc-upload-firmware.py "$@" --id $ESC_ID
#done

#!/bin/bash
#set -e -x

echo "Usage:"
echo "- Autodetect board and upload latest firmware to all ESCs:"
echo "    $0"
echo "- Autodetect board and upload latest firmware to a single ESC ID <esc_id>:"
echo "    $0 <esc_id>"
echo ""

VOXL_ESC_TOOLS_PATH=`pwd`
FIRMWARE_PATH=$VOXL_ESC_TOOLS_PATH/firmware

cd $VOXL_ESC_TOOLS_PATH
#SCAN_OUTPUT=`./voxl-esc-scan.py`
#BOARD=`$SCAN_OUTPUT | grep --text "Board" | head -1`

echo "Detecting ESCs..."

BOARD=$(./voxl-esc-scan.py | grep --text "Board" | head -1)

M0049_FW_FILE="modalai_esc_firmware_m0049"
M0117_1_FW_FILE="modalai_esc_firmware_m0117_1"
M0117_3_FW_FILE="modalai_esc_firmware_m0117_3"
M0129_3_FW_FILE="modalai_esc_firmware_m0129_3"
M0134_1_FW_FILE="modalai_esc_firmware_m0134_1"
M0134_3_FW_FILE="modalai_esc_firmware_m0134_3"
M0134_6_FW_FILE="modalai_esc_firmware_m0134_6"
M0138_1_FW_FILE="modalai_esc_firmware_m0138_1"
TMOTOR_F55A_FW_FILE="modalai_esc_firmware_tmotor_f55a_revc"

ESC_FW="NONE"
HASH="NONE"

if [ -z "$BOARD" ]; then
    echo -e "ERROR: No ESC boards detected.."
    echo -e "Here is output of voxl-esc-scan"
    echo -e ""
    ./voxl-esc-scan.py
	exit 1
fi

if [[ "$BOARD" == *"M0049"* ]]; then
	echo -e "[INFO] M0049 detected"
	ESC_FW=$(ls $FIRMWARE_PATH/ | grep $M0049_FW_FILE)
elif [[ "$BOARD" == *"M0117-1"* ]]; then
	echo -e "[INFO] M0117-1 detected"
	ESC_FW=$(ls $FIRMWARE_PATH/ | grep $M0117_1_FW_FILE)
elif [[ "$BOARD" == *"M0117-3"* ]]; then
	echo -e "[INFO] M0117-3 detected"
	ESC_FW=$(ls $FIRMWARE_PATH/ | grep $M0117_3_FW_FILE)
elif [[ "$BOARD" == *"M0129-3"* ]]; then
	echo -e "[INFO] M0129-3 detected"
	ESC_FW=$(ls $FIRMWARE_PATH/ | grep $M0129_3_FW_FILE)
elif [[ "$BOARD" == *"M0134-1"* ]]; then
	echo -e "[INFO] M0134-1 detected"
	ESC_FW=$(ls $FIRMWARE_PATH/ | grep $M0134_1_FW_FILE)
elif [[ "$BOARD" == *"M0134-3"* ]]; then
	echo -e "[INFO] M0134-3 detected"
	ESC_FW=$(ls $FIRMWARE_PATH/ | grep $M0134_3_FW_FILE)
elif [[ "$BOARD" == *"M0134-6"* ]]; then
	echo -e "[INFO] M0134-6 detected"
	ESC_FW=$(ls $FIRMWARE_PATH/ | grep $M0134_6_FW_FILE)
elif [[ "$BOARD" == *"M0138-1"* ]]; then
	echo -e "[INFO] M0138-1 detected"
	ESC_FW=$(ls $FIRMWARE_PATH/ | grep $M0138_1_FW_FILE)
elif [[ "$BOARD" == *"G071"* ]]; then
	echo -e "[INFO] Tmotor F55A PRO G071 detected"
	ESC_FW=$(ls $FIRMWARE_PATH/ | grep $TMOTOR_F55A_FW_FILE)
elif [[ "$BOARD" == *"F051"* ]]; then
	echo -e "[INFO] Tmotor F55A PRO F051 detected"
elif [[ "$BOARD" == *"M0065"* ]]; then
	echo -e "[INFO] ModalAi M0065 PX4IO (M0065) detected"
	echo -e "Currently no firmware for M0065 in the voxl-esc-tool yet"
	exit 0
else
	echo -e "Unknown board detected: ${BOARD}"
	exit 1
fi

if [ -z "$ESC_FW" ]
then
  echo "\$ESC_FW is empty (firmware not found)"
  exit 1
fi

echo "ESC Firmware: " $ESC_FW

if [ -z "$1" ]
  then
    echo "Flashing all ESCs"
    for ESC_ID in {0..3}
    do
        ./voxl-esc-upload-firmware.py --id $ESC_ID --firmware-file $FIRMWARE_PATH/$ESC_FW
    done
else
    echo "Flashing ESC ID $1"
    ./voxl-esc-upload-firmware.py --id $1 --firmware-file $FIRMWARE_PATH/$ESC_FW
fi


