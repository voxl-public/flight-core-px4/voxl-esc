#!/usr/bin/python3

from crc import crc16
import numpy as np
import serial
import time
import struct
import argparse

from voxl_esc_setup_paths import *
voxl_esc_setup_paths()
from libesc import *
from esc_scanner import EscScanner


ESC_PACKET_HEADER                  = 0xAF
ESC_PACKET_TYPE_LED_RGB_ARRAY_CMD  = 25 #RGB
ESC_PACKET_TYPE_LED_RGBW_ARRAY_CMD = 26 #RGBW



parser = argparse.ArgumentParser(description='ESC LED Test Script')
parser.add_argument('--device',         required=False, default=None)
parser.add_argument('--baud-rate',      required=False, default=None)
parser.add_argument('--mode',           required=False, default="sin")
parser.add_argument('-n', '--num-leds', required=False, default=16,   type=int, help='number of LEDs in the chain')
parser.add_argument('--brightness',     required=False, default=64, type=int, help='brightness from 0 to 255')
parser.add_argument('--is_rgbw',        required=False, default=True)
parser.add_argument('--id',             required=False, default=0)
args = parser.parse_args()

devpath  = args.device
baudrate = args.baud_rate
mode     = args.mode
num_leds = args.num_leds
brightness = args.brightness
is_rgbw  = args.is_rgbw
led_id   = args.id

def wrap_data(packet_type, data):
    data_length = len(data)

    packet_length = data_length + 5 # add bytes for header(1), length(1), type(1), crc(2) 

    packet = [ESC_PACKET_HEADER,
              packet_length,
              packet_type]

    if data_length > 0:
        packet.extend(data)

    crc_val = crc16(packet[1:]) #do not include header byte

    packet = np.uint8(packet)
    packet = np.append(packet, np.frombuffer(crc_val, dtype=np.uint8))

    return packet


class SerialPort:
    dev       = None
    dev_path  = ''
    baud_rate = 115200

    def __init__(self, device_path, baud_rate):
        self.dev_path  = device_path
        self.baud_rate = baud_rate

    def open(self):
        self.dev              = serial.Serial()
        self.dev.port         = self.dev_path
        self.dev.baudrate     = self.baud_rate
        self.dev.parity       = serial.PARITY_NONE
        self.dev.bytesize     = serial.EIGHTBITS
        self.dev.stopbits     = serial.STOPBITS_ONE
        self.dev.timeout      = 0.025
        self.dev.writeTimeout = 0.025

        self.dev.open()
        self.dev.flush()

    def close(self):
        self.dev.flush()
        self.dev.close()

    def flush(self):
        self.dev.flushInput()
        self.dev.flush()

    def write(self, data):
        self.dev.write(data)

    def read(self):
        data_array=''
        if self.dev.inWaiting() > 0:
            data_array = self.dev.read(self.dev.inWaiting())

        return data_array




# quick scan for ESCs to detect the port
scanner = EscScanner()
(devpath, baudrate) = scanner.scan(devpath, baudrate)

if devpath is not None and baudrate is not None:
    print('INFO: ESC(s) detected on port: ' + devpath + ', baud rate: ' + str(baudrate))
else:
    print('ERROR: No ESC(s) detected, exiting.')
    sys.exit(1)


if 'slpi' in devpath:
    from voxl_serial import VoxlSerialPort
    port = VoxlSerialPort()
    port.port = devpath
    port.baudrate = baudrate
    port.open()
else:
    port = SerialPort(devpath,baudrate);
    port.open()


rgb      = np.zeros(1+3*num_leds,dtype=np.uint8)
rgb[0]   = led_id
rgbw     = np.zeros(1+4*num_leds,dtype=np.uint8)
rgbw[0]  = led_id
cntr     = 0
fps      = 50  # rate of sending LED commands
sw_freq  = 0.5 # relative frequency for switching between R, G, B, W



while(1):

    if (mode == "sin"):
        cntr       = cntr + 1
        cycles     = 1 # how many sine cycles
        resolution = num_leds # how many datapoints to generate
        offset     = cntr / 20.0 * np.pi  #set to zero for static wave

        length     = np.pi * 2 * cycles
        red_wave   = (1.0 + np.sin(np.arange(0, length, length / resolution) + offset)) * brightness
        green_wave = red_wave
        blue_wave  = red_wave
        white_wave = red_wave

        if (cntr*sw_freq % (fps*4) < fps):
            red_on = 1; green_on = 0; blue_on = 0; white_on = 0
        elif (cntr*sw_freq % (fps*4) < fps*2):
            red_on = 0; green_on = 1; blue_on = 0; white_on = 0
        elif (cntr*sw_freq % (fps*4) < fps*3):
            red_on = 0; green_on = 0; blue_on = 1; white_on = 0
        else:
            red_on = 0; green_on = 0; blue_on = 0; white_on = 1

        for idx in range(num_leds):
            rgb[1+idx*3]  = red_on*red_wave[idx]
            rgb[2+idx*3]  = green_on*green_wave[idx]
            rgb[3+idx*3]  = blue_on*blue_wave[idx]

            rgbw[1+idx*4] = red_on*red_wave[idx]
            rgbw[2+idx*4] = green_on*green_wave[idx]
            rgbw[3+idx*4] = blue_on*blue_wave[idx]
            rgbw[4+idx*4] = white_on*white_wave[idx]

    elif (mode == "white"):

        for idx in range(num_leds):
            rgb[1+idx*3]  = brightness
            rgb[2+idx*3]  = brightness
            rgb[3+idx*3]  = brightness

            rgbw[1+idx*4] = 0
            rgbw[2+idx*4] = 0
            rgbw[3+idx*4] = 0
            rgbw[4+idx*4] = brightness

    elif (mode == "all"):

        for idx in range(num_leds):
            rgb[1+idx*3]  = brightness
            rgb[2+idx*3]  = brightness
            rgb[3+idx*3]  = brightness

            rgbw[1+idx*4] = brightness
            rgbw[2+idx*4] = brightness
            rgbw[3+idx*4] = brightness
            rgbw[4+idx*4] = brightness
    else:
        print("ERROR mode must be sin, white, or all")
        sys.exit(1)


    if is_rgbw:
        packet = wrap_data(ESC_PACKET_TYPE_LED_RGBW_ARRAY_CMD,rgbw)
    else:
        packet = wrap_data(ESC_PACKET_TYPE_LED_RGB_ARRAY_CMD,rgb)

    print(packet)


    port.write(packet);
    time.sleep(1.0 / fps)
