<!--
Note, tone power of 12 is the lowest that can be heard on M0117
13 is the lowest that can be heard on M0129 -->

    <!-- The Entertainer -->
    <param name="tone_freqs"          value="[118, 132, 105, 88, 99, 88, 83, 78, 1, 235, 0, 0]"/> <!-- 200 is 2000Hz, max 255 -->
    <param name="tone_durations"      value="[30, 30, 30, 60, 30, 30, 30, 60, 60, 60, 0, 0]"/> <!-- duration of each tone in units of 10 milli-seconds. Poor naming!!! -->
    <param name="tone_powers"         value="[15, 15, 15, 15, 15, 15, 15, 15, 0, 15, 0, 0]"/> <!-- max is 255 -->




    <!--Ride of the Valkyries-->
    <param name="tone_freqs"          value="[118, 99, 118, 140, 70, 99, 70, 99, 118, 0, 0, 0]"/> <!-- 200 is 2000Hz, max 255 -->
    <param name="tone_durations"      value="[50, 30, 40, 100, 100, 50, 30, 40, 160, 0, 0, 0]"/> <!-- duration of each tone in units of 10 milli-seconds. Poor naming!!! -->
    <param name="tone_powers"         value="[25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 0, 0]"/> <!-- max is 255 -->


    <!--simple defualt chime-->
    <param name="tone_freqs"          value="[200, 215, 225, 250, 0,0,0,0, 0,0,0,0]"/> <!-- 200 is 2000Hz, max 255 -->
    <param name="tone_durations"      value="[10,  10,  10,  10,  0,0,0,0, 0,0,0,0]"/> <!-- duration of each tone in units of 10 milli-seconds. Poor naming!!! -->
    <param name="tone_powers"         value="[60,  60,  60,  60,  0,0,0,0, 0,0,0,0]"/> <!-- max is 255 -->

    <!--one beep-->
    <param name="tone_freqs"          value="[200, 0,0,0, 0,0,0,0, 0,0,0,0]"/> <!-- 200 is 2000Hz, max 255 -->
    <param name="tone_durations"      value="[10,  0,0,0, 0,0,0,0, 0,0,0,0]"/> <!-- duration of each tone in units of 10 milli-seconds. Poor naming!!! -->
    <param name="tone_powers"         value="[40,  0,0,0, 0,0,0,0, 0,0,0,0]"/> <!-- max is 255 -->


    <!--Silent-->
    <param name="tone_freqs"          value="[200, 215, 225, 250, 0,0,0,0, 0,0,0,0]"/> <!-- 200 is 2000Hz, max 255 -->
    <param name="tone_durations"      value="[0,0,0,0,  0,0,0,0, 0,0,0,0]"/> <!-- duration of each tone in units of 10 milli-seconds. Poor naming!!! -->
    <param name="tone_powers"         value="[60,  60,  60,  60,  0,0,0,0, 0,0,0,0]"/> <!-- max is 255 -->
